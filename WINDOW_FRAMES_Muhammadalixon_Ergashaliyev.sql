/* --Task Description
1. You need to construct a query that meets the following requirements:
2. Generate a sales report for the 49th, 50th, and 51st weeks of 1999.
3. Include a column named CUM_SUM to display the amounts accumulated during each week.
4. Include a column named CENTERED_3_DAY_AVG to show the average sales for the previous, current, and following days using a centered moving average.
5. For Monday, calculate the average sales based on the weekend sales (Saturday and Sunday) as well as Monday and Tuesday.
6. For Friday, calculate the average sales on Thursday, Friday, and the weekend.
*/

WITH week_sales AS (
    SELECT
        t.time_id,
        t.day_number_in_week,
        t.calendar_week_number,
        t.day_name,
        SUM(amount_sold) AS sales_amount
    FROM
        sh.sales
        JOIN sh.times t USING(time_id)
    WHERE
        t.calendar_week_number IN (49, 50, 51)
        AND EXTRACT(YEAR FROM t.time_id) = '1999'
    GROUP BY
        t.time_id,
        t.calendar_week_number,
        t.day_name
    ORDER BY
        time_id
)

SELECT
    time_id,
    day_name,
    calendar_week_number,
    sales_amount,
    SUM(sales_amount) OVER (PARTITION BY calendar_week_number ORDER BY time_id) AS cum_sum,
    ROUND(
        AVG(sales_amount) OVER (ORDER BY time_id ROWS BETWEEN 2 PRECEDING AND 1 FOLLOWING),
        2
    ) AS centered_3_day_avg
FROM
    week_sales;